#ifndef __symboling_RecordType__
#define __symboling_RecordType__

#include <math.h>
#include "Type.hpp"

namespace symboling {
  /**
   * @class RecordType
   * @description Specifies a record type.
   * @extends Type
   *
   * <a name="parameters"></a>
   * ## Type parameters
   * ### General parameters
   * - `name: ...` The type name. Required value.
   * - `types: { name: type, ...}` The type of each record item. By default, the name itself is considered as the type name.
   * - `strict`: If true throws an error on undefined types, default is `false`.
   * ### Syntactic projection parameters
   * - `required: [ name ...]` required record field, such field must be defined for the record to be valid.
   * - `forbidden: [ name ...]` forbidden record field, such field must not be defined for the record to be valid.
   * ### Parameters for the distance calculation
   * - `weight: { name: weight_value ...}` The distance weights when adding all record value.
   *   - Default weight value is `1`.
   * ### Parameters for string parsing
   * If the input value is a string, there is a possibility to parse this value in order to construct a record, using [`getValue()`](#getValue).
   * - `parseInput` The regex defining the syntax input.
   * - `parseOutput` The wjson pattern defining the syntax output.
   *
   * <a name="specification"></a>
   * ## RecordType specification
   * <table class="specification">
   * <tr style='border: 1px solid black;'><th> Syntactic projection </th><td>
   * - If a required field is missing its definition is forced using an empty value.
   * <br/>- If a forbidden field is present its definition is erased.
   * <br/> - Each record item is syntactically projected.
   * <br/>Furthermore:
   * <br/>- The value can be parsed using a specific syntax managed by <a href="https://line.gitlabpages.inria.fr/aide-group/aidesys/regex.html#.regexReplace">regex replacement</a>.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Semantic projection  </th><td>
   * - Each record item is semantically projected.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Distance value       </th><td>
   * - The distance is computed as the weighted sum of each record item.
   * <br/>- The distance is computed against the empty value if one record item is missing.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Geodesic path        </th><td>
   * - The path is constructed in the record item order scanning simultaneously both record item list.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Comparison           </th><td>
   * - The comparison is performed in the record item order scanning simultaneously both record item list.
   * <br/> - Returns <tt>0</tt> If each element pair compares equal and both lists have the same length.
   * <br/> - Returns <tt><0</tt> If either the value of the first item that does not match is lower in the left hand side list, or all compared items match but the left hand side list is shorter.
   * <br/> - Returns <tt>>0</tt> If either the value of the first item that does not match is greater in the left hand side list, or all compared items match but the left hand side list is longer.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Bounds           </th><td>
   * - Considers all required items bounds to calculate the recod bounds.
   * </td></tr>
   * </table>
   *
   * @param {JSON|String} parameters The type parameters.
   *
   */
  class RecordType: public Type {
    bool strict = false;
    // Concatenation of both record names
    mutable wjson::Value names;
    const Type& getType(String name) const;
public:
    RecordType(JSON parameters);
    RecordType(String parameters);
    RecordType(const char *parameters);

    /**
     * @function getValue
     * @memberof Type
     * @instance
     * @description Parses a string in order to decode its elements as a record item.
     * - The input string is parsed as a record, when inputing a string of a suitable format.
     * - The implementation uses the [`aidesys::regexReplace()`](https://line.gitlabpages.inria.fr/aide-group/aidesys/regex.html#.regexReplace) method to:
     *    - Extract values from the input string using capturing parenthesis of the regex.
     *    - Substitute the captured value in a wJSON syntax representation of the data structure,
     * As illustrated by this example:
     * ```
     * static symboling::RecordType dateStandardFormat("{"
     * "name: date "
     * " parseInput: '([0-9]*)-([0-9]*)-([0-9]*)' "
     * " parseOutput: '{year: $1 month: $2 day: $3}' }");
     * ```
     * which corresponds to recommended international standard format for dates.
     *
     * @param {String} value The input value.
     * @param {bool} [semantically_else_syntactically=true]
     * - If true projects the value onto a semantically valid value with respect to this type.
     * - If false projects the value onto a syntactically valid value with respect to this type.
     * @return {Value} The projection of the value onto the a value of this type:
     * - If the value is already of this type it is unchanged.
     * - If the value can be mapped on a value of this type, then it is returned.
     * - Otherwise a default "undefined" value or an empty value is returned.
     */
    virtual wjson::Value getValue(String value, bool semantically_else_syntactically = true) const;
    virtual wjson::Value getValue(const char *value, bool semantically_else_syntactically = true) const;

    virtual wjson::Value getValue(JSON value, bool semantically_else_syntactically = true) const;
    virtual double getDistance(JSON lhs, JSON rhs) const;
    virtual wjson::Value getPath(JSON lhs, JSON rhs) const;
    virtual double compare(JSON lhs, JSON rhs) const;
    virtual JSON getBound(unsigned int index = 0) const;
    virtual String asString() const;
  };
}

#endif
