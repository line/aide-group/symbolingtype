#include "symbolingtype.hpp"

namespace symboling {
  void setMidiType()
  // Implements, as symboling types, a segment of the MIDI https://www.midi.org specification:
  {
    static NumericType midi_numeric("{name: midi_numeric strict step: 1}");

    static RecordType midi_note("{name: midi_note strict types: {midi: midi_numeric  time: midi_numeric ticks: midi_numeric name: string pitch: string octave: midi_numeric velocity: midi_numeric duration: midi_numeric durationTicks: midi_numeric}}");

    static ListType midi_notes("{name: midi_notes strict itemType: midi_note}");

    static RecordType midi_controlChanges("{name: midi_controlChanges strict: false types: {number: midi_numeric ticks: midi_numeric time: midi_numeric value: midi_numeric}}");

    static ListType midi_controlChangeList("{name: midi_controlChangeList strict itemType: midi_controlChanges}");

    static RecordType midi_endOfTrackTicks("{name: midi_endOfTrackTicks strict: false types: {}}");

    static RecordType midi_instrument("{name: midi_instrument strict types: {number: midi_numeric family: string name: string percussion: modal}}");

    static RecordType midi_track("{name: midi_track strict types: {name: string channel: midi_numeric controlChanges: midi_controlChangeList instrument: midi_instrument endOfTrackTicks: midi_numeric pitchBends: string notes: midi_notes}}");

    static RecordType midi_keySignature("{name: midi_keySignature strict: false types: {}}");

    static ListType midi_keySignatures("{name: midi_keySignatures strict itemType: midi_keySignature}");

    static RecordType midi_timeSignature("{name: midi_timeSignature strict: false types: {}}");

    static ListType midi_timeSignatures("{name: midi_timeSignatures strict itemType: midi_timeSignature}");

    static RecordType midi_header("{name: midi_header strict types: {name: string tempos: string timeSignatures: midi_timeSignatures ppq: midi_numeric keySignatures: midi_keySignatures meta: string }}");

    static ListType midi_tracks("{name: midi_tracks strict itemType: midi_track}");

    static RecordType midi_score("{name: midi_score strict types: {header: midi_header duration: midi_numeric tracks: midi_tracks}}");
  }
}
