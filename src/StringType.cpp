#include "StringType.hpp"
#include "regex.hpp"

namespace symboling {
  static bool StringTypeRegistered = Type::addType("StringType", [] (JSON parameters) { return new StringType(parameters);
                                                   });
  StringType::StringType(JSON parameters) : ListType(parameters.clone().set("name", parameters.get("name", "string")).set("parent", parameters.isMember("name") ? "string" : "").set("editCost", parameters.get("editCost", 1)).set("deleteCost", parameters.get("deleteCost", 1)).set("insertCost", parameters.get("insertCost", parameters.get("deleteCost", 1)))), checkPattern(parameters.get("checkPattern", "")), atomic(parameters.get("atomic", true))
  {
    // Creates the bounds
    if(parameters.isMember("bounds")) {
      bounds = parameters.at("bounds");
      bounds_count = bounds.length();
    }
  }
  StringType::StringType(String parameters) : StringType(wjson::string2json(parameters))
  {}
  StringType::StringType(const char *parameters) : StringType(wjson::string2json(parameters))
  {}
  StringType::StringType() : StringType("")
  {}

  wjson::Value StringType::getValue(JSON value, bool semantically_else_syntactically) const
  {
    startCheckMessage();
    ListType::getValue(string2chars(value), semantically_else_syntactically);
    if(value.isRecord()) {
      addCheckMessage(value, "not a string, but a structured value");
    }
    if(checkPattern != "" && !aidesys::regexMatch(value.asString(), checkPattern)) {
      addCheckMessage(value, "incorrect syntax, with respect to the checkPattern regex");
    }
    stopCheckMessage();
    return value.asString();
  }
  double StringType::getDistance(JSON lhs, JSON rhs) const
  {
    return ListType::getDistance(string2chars(lhs), string2chars(rhs));
  }
  wjson::Value StringType::getPath(JSON lhs, JSON rhs) const
  {
    // Returns an atomic path [lhs, rhs]
    if(atomic) {
      return Type::getPath(lhs, rhs);
    }
    // Delegates the path calculation as list of chars
    JSON path = ListType::getPath(string2chars(lhs), string2chars(rhs));
    // Back converts list of chars to string
    wjson::Value result;
    for(unsigned int i = 0; i < path.length(); i++) {
      result[i] = chars2string(path.at(i));
    }
    return result;
  }
  double StringType::compare(JSON lhs, JSON rhs) const
  {
    return lhs.get("").compare(rhs.get(""));
  }
  JSON StringType::getBound(unsigned int index) const
  {
    return Type::getBound(index);
  }
  wjson::Value StringType::string2chars(JSON value)
  {
    std::string string = value.get("");
    wjson::Value result;
    for(unsigned int i = 0; i < string.length(); i++) {
      result[i] = string.at(i);
    }
    return result;
  }
  std::string StringType::chars2string(JSON value)
  {
    std::string result;
    for(unsigned int i = 0; i < value.length(); i++) {
      result.append(value.at(i).asString());
    }
    return result;
  }
}
