#include "Type.hpp"
#include "StringType.hpp"
#include "NumericType.hpp"
#include "ModalType.hpp"
#include "EnumType.hpp"
#include "ListType.hpp"
#include "SetType.hpp"
#include "RecordType.hpp"
#include "SvgType.hpp"
#include "MidiType.hpp"
#include <memory>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <cctype>
#include "regex.hpp"
#include <algorithm>    // std::shuffle
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock

namespace symboling {
  // Avoids copy of types, also could be implemented at complitation time in Type.hpp using = delete but less explicit
  Type::Type(const Type& obj)
  {
    aidesys::alert(true, "illegal-state", "in symboling::Type the source code is trying to copy a Type which is an ill-defined operation");
  }
  Type& Type::operator = (const Type& type) {
    aidesys::alert(true, "illegal-state", "in symboling::Type the source code is trying to copy a Type which is an ill-defined operation");
    return *this;
  }
  // Registration mechanism of types
  void initDefaultTypes()
  {
    static bool tobedone = true;
    if(tobedone) {
      tobedone = false;
      static Type valueType("{name: value}");
      static ModalType modalType;
      static StringType stringType;
      extern void setSvgType();
      setSvgType();
      extern void setMidiType();
      setMidiType();
    }
  }
  std::map < std::string, Type * >& Type::getTypes() {
    initDefaultTypes();
    static std::map < std::string, Type * > types;
    return types;
  }
  const Type& Type::getType(String name)
  {
    const std::map < std::string, Type * >& types = Type::getTypes();
    std::map < std::string, Type * > ::const_iterator it = types.find(name);
    return *(it == types.end() ? Type::getTypes().at("value") : it->second);
  }
  wjson::Value Type::getTypeNames()
  {
    wjson::Value result;
    for(auto it = getTypes().begin(); it != getTypes().end(); it++) {
      result.add(it->first);
    }
    return result;
  }
  wjson::Value Type::getTypeParameters()
  {
    wjson::Value result;
    for(auto it = getTypes().begin(); it != getTypes().end(); it++) {
      result.set(it->first, it->second->getParameters());
    }
    return result;
  }
  void Type::addType(Type *type)
  {
    // Here creates a mechanism to delete all allocated type objects at shutdown
    class Types {
      // Stores the type pointers and delete them in the object destructor
public:
      std::vector < Type * > types;
      ~Types() {
        for(auto it = types.begin(); it != types.end(); it++) {
          try {
            delete *it;
            // Attemps to avoid exeception if the type is already deleted
          }
          catch(std::exception& e) {}
        }
      }
    };
    // Uses unique_ptr mechanism to delete the object at shutdown as a static variable
    static Types *remament_types = new Types();
    static std::unique_ptr < Types > ptr_type = std::unique_ptr < Types > (remament_types);
    // Registers the type for deletion
    remament_types->types.push_back(type);
  }
  static std::string str_tolower(std::string s)
  {
    std::transform(s.begin(), s.end(), s.begin(), [] (unsigned char c) {
      return std::tolower(c);
    });
    return s;
  }
  void Type::addType(JSON type)
  {
    if(type.isArray()) {
      for(unsigned int i = 0; i < type.length(); i++) {
        addType(type.at(i));
      }
    } else {
      std::string t = aidesys::regexReplace(str_tolower(type.get("type", "")), "[_-]*type$", "");
      printf("Adding from json type '%s' from type parent '%s'\n", type.asString().c_str(), t.c_str());
      auto it = typeFactory.find(t);
      if(it != typeFactory.cend()) {
        addType(it->second(type));
      } else {
        aidesys::alert(true, "illegal-argument", "in Symboling::Type::addType undefined parent type: '" + t + "' from '" + type.asString() + "'");
      }
    }
  }
  void Type::addType(String type)
  {
    addType(wjson::string2json(type));
  }
  void Type::addType(const char *type)
  {
    addType(wjson::string2json(type));
  }
  bool Type::addType(String name, std::function < Type *(JSON) > type)
  {
    std::string t = aidesys::regexReplace(str_tolower(name), "[_-]*type$", "");
    printf("Adding type '%s' implemented as '%s'\n", t.c_str(), name.c_str());
    typeFactory.insert(std::pair < String, std::function < Type * (JSON) >> (t, type));
    return true;
  }
  std::map < std::string, std::function < Type * (JSON) >> Type::typeFactory;
  // Registers the type at the object construction
  static bool ValueTypeRegistered = Type::addType("value", [] (JSON parameters) { return new Type(parameters);
                                                  });
  Type::Type(JSON parameters) : parameters(parameters)
  {
    // Checks that a name is defined in the parameter data structure
    aidesys::alert(parameters.at("name").isEmpty() || parameters.get("name", "") == "", "illegal-argument", "in symboling::Type::Type type parameters without a name '%s'", parameters.asString().c_str());
    // Checks that the type is not yet defined
    {
      std::map < std::string, Type * > types = Type::getTypes();
      std::map < std::string, Type * > ::const_iterator it = types.find(parameters.get("name", ""));
      aidesys::alert(it != types.end(), "illegal-argument", "in symboling::Type::Type type %s is already defined", parameters.asString().c_str());
    }
    // Registers the type
    getTypes()[parameters.get("name", "")] = this;
    // Creates the bound singleton
    bounds[0] = wjson::Value::EMPTY;
    bounds_count = 1;
  }
  Type::Type(String parameters) : Type(wjson::string2json(parameters))
  {}
  Type::Type(const char *parameters) : Type(wjson::string2json(parameters))
  {}
  // Checks message global variables and mechanisms
  std::string Type::check_message = "";
  unsigned int Type::check_message_recursion_level = 0;
  void Type::startCheckMessage()
  {
    if(check_message_recursion_level == 0) {
      check_message = "";
    }
    check_message_recursion_level++;
  }
  void Type::stopCheckMessage()
  {
    check_message_recursion_level--;
  }
  void Type::addCheckMessage(JSON value, String explanation, ...) const
  {
    // Parses the message à-la-printf
    const unsigned int message_buffer_size = 10000;
    char chars_buffer[message_buffer_size];
    {
      va_list a;
      va_start(a, explanation);
      vsnprintf(chars_buffer, message_buffer_size, explanation.c_str(), a);
      va_end(a);
    }
    check_message += "// The '" + value.asString() + "' is not of type '" + getParameters().asString() + "': " + chars_buffer + ".\n";
  }
  // Cache mechanism of getDistance management
  bool Type::justDone(JSON lhs, JSON rhs) const
  {
    if(&last_lhs == &lhs && &last_rhs == &rhs && already_set) {
      return true;
    } else {
      last_distance = 0;
      return false;
    }
  }
  void Type::setDistance(JSON lhs, JSON rhs, double distance) const
  {
    last_lhs = lhs, last_rhs = rhs, last_distance = distance, already_set = true;
  }
  // Default implementations of Type virtual methods
  wjson::Value Type::getValue(JSON value, bool semantically_else_syntactically) const
  {
    return value;
  }
  wjson::Value Type::getValue(String value, bool semantically_else_syntactically) const
  {
    return getValue(wjson::string2json(value), semantically_else_syntactically);
  }
  wjson::Value Type::getValue(const char *value, bool semantically_else_syntactically) const
  {
    return getValue(wjson::string2json(value), semantically_else_syntactically);
  }
  double Type::getDistance(JSON lhs, JSON rhs) const
  {
    return last_distance;
  }
  wjson::Value Type::getPath(JSON lhs, JSON rhs) const
  {
    wjson::Value path;
    path[0] = lhs;
    if(lhs != rhs) {
      path[1] = rhs;
    }
    return path;
  }
  double Type::compare(JSON lhs, JSON rhs) const
  {
    return NAN;
  }
  JSON Type::getBound(unsigned int index) const
  {
    return bounds.at(index);
  }
  JSON Type::getBounds(unsigned int new_bounds_count, bool reset) const
  {
    if(last_bounds_count != new_bounds_count || reset) {
      last_bounds_count = new_bounds_count;
      last_bounds.clear();
      if(new_bounds_count == 0 || bounds_count <= new_bounds_count) {
        // Selects all elements
        for(unsigned int i = 0; i < bounds_count; i++) {
          last_bounds[i] = getBound(i);
        }
      } else if(new_bounds_count <= 2 * bounds_count) {
        // Selects a subset by index shuffling, if many elements to randomly choose
        unsigned int indexes[bounds_count];
        for(unsigned int i = 0; i < bounds_count; indexes[i] = i, i++) {}
        std::shuffle(indexes, indexes + bounds_count, std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count()));
        for(unsigned int i = 0; i < new_bounds_count; i++) {
          last_bounds[i] = getBound(indexes[i]);
        }
      } else {
        // Selects elements by random selection and increment until a new element is found
        std::unordered_map < unsigned int, bool > indexes;
        srand(time(NULL));
        for(unsigned int i = 0; i < new_bounds_count; i++) {
          unsigned int index = rand() % bounds_count;
          for(unsigned int n = 0; n < bounds_count && indexes.find(index) != indexes.end(); n++) {
            index = (index + 1) % bounds_count;
          }
          indexes[index] = true;
          last_bounds[i] = getBound(index);
        }
      }
    }
    return last_bounds;
  }
  String Type::asString() const
  {
    static std::string string;
    string = "{ symboling::_" + parameters.get("name", "") + "_Type: " + parameters.asString();
    if(!std::isnan(last_distance)) {
      string += " justDone: { lhs: " + getValue(last_lhs).asString() + " rhs: " + getValue(last_rhs).asString() + aidesys::echo(" distance: %f", last_distance);
    }
    string += "}";
    return string;
  }
}
