#include "RecordType.hpp"
#include "regex.hpp"

namespace symboling {
  static bool RecordTypeRegistered = Type::addType("RecordType", [] (JSON parameters) { return new RecordType(parameters);
                                                   });
  RecordType::RecordType(JSON parameters) : Type(parameters)
  {
    strict = getParameters().get("strict", false);
    bool i = getParameters().isMember("parseInput"), o = getParameters().isMember("parseOutput");
    aidesys::alert((i && !o) || (!i && o), "illegal-argument", "in symboling::RecordType::RecordType parseInput and parseOutput are not specified together: '" + getParameters().asString() + "'");
    try {
      aidesys::regexReplace("", getParameters().get("parseInput", ""), getParameters().get("parseOutput", ""));
    }
    catch(std::exception& e) {
      aidesys::alert(true, "illegal-argument", "in symboling::RecordType::RecordType bad regex syntax: '%s'", e.what());
    }
    // Calculates bounds_count
    if(parameters.isMember("required")) {
      bounds_count = 1;
      double log_bounds_count = 0;
      for(unsigned int i = 0; i < parameters.at("required").length(); i++) {
        unsigned int c = getType(parameters.at("required").get(i, "value")).getBoundsCount();
        bounds_count *= c, log_bounds_count += log(c);
      }
      // Bounds count overflows (higher than 2^64 > e^44.361418)
      if(log_bounds_count > 44.361418) {
        bounds_count = 0;
      }
    }
  }
  RecordType::RecordType(String parameters) : RecordType(wjson::string2json(parameters))
  {}
  RecordType::RecordType(const char *parameters) : RecordType(wjson::string2json(parameters))
  {}
  const Type& RecordType::getType(String name) const
  {
    return Type::getType(getParameters().at("types").isMember(name) ? getParameters().at("types").get(name, "") : name);
  }
  wjson::Value RecordType::getValue(JSON value, bool semantically_else_syntactically) const
  {
    startCheckMessage();
    wjson::Value result = value;
    if(value.isRecord()) {
      if(getParameters().isMember("required")) {
        JSON required = getParameters().at("required");
        for(unsigned int i = 0; i < required.length(); i++) {
          std::string name = required.at(i).asString();
          if(!value.isMember(name)) {
            addCheckMessage(value, "required field '" + name + "' missing");
            result[name] = wjson::Value::EMPTY;
          }
        }
      }
      if(getParameters().isMember("forbidden")) {
        JSON forbidden = getParameters().at("forbidden");
        for(unsigned int i = 0; i < forbidden.length(); i++) {
          std::string name = forbidden.at(i).asString();
          if(value.isMember(name)) {
            addCheckMessage(value, "forbidden field '" + name + "' present");
            result.erase(name);
          }
        }
      }
      for(auto it = value.getNames().begin(); it != value.getNames().end(); ++it) {
        String name = *it;
        if(RecordType::getType(name).getParameters().isMember("name")) {
          if(RecordType::getType(name).getParameters().at("name") == "value") {
            addCheckMessage(value, "the '" + name + "' type '" + (getParameters().at("types").isMember(name) ? getParameters().at("types").get(name, "") : name) + "' has not be defined");
          }
        } else {
          addCheckMessage(value, "the '" + name + "' has spurious type with no name, this may be symboling bug");
        }
      }
      for(auto it = value.getNames().begin(); it != value.getNames().end(); ++it) {
        String name = *it;
        result[name] = RecordType::getType(name).getValue(value.at(name), semantically_else_syntactically);
      }
    } else {
      addCheckMessage(value, "not a record");
    }
    stopCheckMessage();
    return result;
  }
  wjson::Value RecordType::getValue(String value, bool semantically_else_syntactically) const
  {
    if(getParameters().isMember("parseInput") && aidesys::regexMatch(value, getParameters().get("parseInput", ""))) {
      return Type::getValue(aidesys::regexReplace(value, getParameters().get("parseInput", ""), getParameters().get("parseOutput", "")), semantically_else_syntactically);
    } else {
      return Type::getValue(value, semantically_else_syntactically);
    }
  }
  wjson::Value RecordType::getValue(const char *value, bool semantically_else_syntactically) const
  {
    return getValue((String) value, semantically_else_syntactically);
  }
  double RecordType::getDistance(JSON lhs, JSON rhs) const
  {
    if(Type::justDone(lhs, rhs)) {
      return Type::getDistance(lhs, rhs);
    }
    // Registers the record names
    names.clear();
    auto it_lhs = lhs.getNames().cbegin(), it_rhs = rhs.getNames().cbegin();
    for(bool loop = true; loop;) {
      loop = false;
      if(it_lhs != lhs.getNames().cend()) {
        if(!names.isMember(*it_lhs)) {
          names[*it_lhs] = true;
        }
        it_lhs++, loop = true;
      }
      if(it_rhs != rhs.getNames().cend()) {
        if(!names.isMember(*it_rhs)) {
          names[*it_rhs] = true;
        }
        it_rhs++, loop = true;
      }
    }
    // Calculates the additive distance over the different items
    double distance = 0;
    for(auto it = names.getNames().cbegin(); it != names.getNames().cend(); it++) {
      const Type& type = RecordType::getType(*it);
      if(type.getParameters().get("name", "") != "value" || strict) {
        double d = type.getDistance(type.getValue(lhs.at(*it)), type.getValue(rhs.at(*it)));
        aidesys::alert(std::isnan(d), "illegal-argument", "in symboling::RecordType::getDistance d returns NAN, for item '" + *it + "' of type '" + type.getParameters().get("name", "") + "' strict: %d", strict);
        distance += getParameters().at("weight").get(*it, 1.0) * d;
      }
    }
    Type::setDistance(lhs, rhs, distance);
    return distance;
  }
  wjson::Value RecordType::getPath(JSON lhs, JSON rhs) const
  {
    if(!Type::justDone(lhs, rhs)) {
      RecordType::getDistance(lhs, rhs);
    }
    wjson::Value path;
    unsigned int k = 0;
    path[k++] = lhs;
    for(auto it = names.getNames().begin(); it != names.getNames().end(); ++it) {
      String name = *it;
      const Type& type = RecordType::getType(name);
      if(rhs.isMember(name)) {
        if(!lhs.isMember(name)) {
          // Inserts a value, appending all intermediate values
          JSON path_name = type.getPath(wjson::Value::EMPTY, rhs.at(name));
          for(unsigned int l = 0; l < path_name.length(); l++) {
            wjson::Value path_k = path[k - 1];
            path_k.set(name, path_name.at(l));
            if(path_k != path[k - 1]) {
              path[k++] = path_k;
            }
          }
        } else if(lhs.at(name) != rhs.at(name)) {
          // Replaces a value, appending all intermediate values
          JSON path_name = type.getPath(lhs.at(name), rhs.at(name));
          for(unsigned int l = 0; l < path_name.length(); l++) {
            wjson::Value path_k = path[k - 1];
            path_k.set(name, path_name.at(l));
            if(path_k != path[k - 1]) {
              path[k++] = path_k;
            }
          }
        }
      } else {
        if(lhs.isMember(name)) {
          // Deletes value, appending all intermediate values
          JSON path_name = type.getPath(lhs.at(name), wjson::Value::EMPTY);
          for(unsigned int l = 0; l < path_name.length(); l++) {
            wjson::Value path_k = path[k - 1];
            path_k.set(name, path_name.at(l));
            if(path_k != path[k - 1]) {
              path[k++] = path_k;
            }
          }
        } else {
          aidesys::alert(true, "illegal-state", "in symboling::RecordType::getPath, spurious name '" + name + "'");
        }
      }
    }
    aidesys::alert(compare(rhs, path[path.length() - 1]) != 0, "illegal-state", "in symboling::RecordType::getPath, spurious final final path value != lhs:\n  '" + lhs.asString() + "' rhs:\n  '" + rhs.asString() + "' !=\n  '" + path[path.length() - 1].asString() + "':\n" + path.asString(true));
    return path;
  }
  double RecordType::compare(JSON lhs, JSON rhs) const
  {
    wjson::Value names;
    const std::vector < std::string >& names_lhs = lhs.getNames(), names_rhs = rhs.getNames();
    std::vector < std::string > ::const_iterator it_lhs = names_lhs.begin(), it_rhs = names_rhs.begin();
    for(bool loop = true; loop;) {
      loop = false;
      if(it_lhs != names_lhs.end()) {
        String name = *it_lhs;
        const Type& type = RecordType::getType(name);
        if(!names.isMember(name)) {
          names[name] = true;
          if(type.getParameters().get("name", "") != "value" || strict) {
            double c = type.compare(type.getValue(lhs.at(name)), type.getValue(rhs.at(name)));
            aidesys::alert(std::isnan(c), "illegal-argument", "in symboling::RecordType::compare the item of name '" + name + "' has an undefined type, in type '" + getParameters().get("name", "") + "'");
            if(c != 0) {
              return c;
            }
          }
        }
        loop = true;
        it_lhs++;
      }
      if(it_rhs != names_rhs.end()) {
        String name = *it_rhs;
        const Type& type = RecordType::getType(name);
        if(!names.isMember(name)) {
          names[name] = true;
          if(type.getParameters().get("name", "") != "value" || strict) {
            double c = type.compare(type.getValue(lhs.at(name)), type.getValue(rhs.at(name)));
            aidesys::alert(std::isnan(c), "illegal-argument", "in symboling::RecordType::compare the item of name '" + name + "' has an undefined type, in type '" + getParameters().get("name", "") + "'");
            if(c != 0) {
              return c;
            }
          }
        }
        loop = true;
        it_rhs++;
      }
    }
    return 0;
  }
  JSON RecordType::getBound(unsigned int index) const
  {
    static wjson::Value bound;
    bound.clear();
    JSON p = getParameters().at("required");
    for(unsigned int i = 0; i < p.length(); i++) {
      String n = p.get(i, "value");
      const Type& t = getType(n);
      unsigned int c = t.getBoundsCount();
      unsigned index_i = index % c;
      bound[n] = t.getBound(index_i);
      index /= c;
    }
    return bound;
  }
  String RecordType::asString() const
  {
    static std::string string;
    string = Type::asString();
    string = string.substr(0, string.length() - 1);
    if(names.size() > 0) {
      string += " names: [ ";
      for(auto it = names.getNames().begin(); it != names.getNames().end(); ++it) {
        string += (*it) + " ";
      }
      string += "]";
    }
    string += "}";
    return string;
  }
}
