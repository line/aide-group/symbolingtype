#include "NumericType.hpp"
#include <cmath>
#include "std.hpp"

namespace symboling {
  static bool NumericTypeRegistered = Type::addType("NumericType", [] (JSON parameters) { return new NumericType(parameters);
                                                    });
  NumericType::NumericType(JSON parameters) : Type(parameters.clone().set("parent", parameters.isMember("name") ? "numeric" : "")), stepsolver::Numeric(parameters), atomic(parameters.get("atomic", true))
  {
    path_step = std::isnan(precision) || precision == 0 ? NAN : fabs(precision);
    // Creates the bounds
    if(parameters.isMember("min") || parameters.isMember("max")) {
      bounds.clear();
      bounds_count = 0;
      if(parameters.isMember("min")) {
        bounds.add(parameters.at("min")), bounds_count++;
      }
      if(parameters.isMember("max")) {
        bounds.add(parameters.at("max")), bounds_count++;
      }
    }
  }
  NumericType::NumericType(String parameters) : NumericType(wjson::string2json(parameters, true))
  {}
  NumericType::NumericType(const char *parameters) : NumericType(wjson::string2json(parameters, true))
  {}

  wjson::Value NumericType::getValue(JSON value, bool semantically_else_syntactically) const
  {
    return getValueAsDouble(value, semantically_else_syntactically);
  }
  double NumericType::getValueAsDouble(JSON value, bool semantically_else_syntactically) const
  {
    startCheckMessage();
    double result;
    std::string string = value.asString();
    if(string == "") {
      addCheckMessage(value, "empty value");
      result = zero;
    } else {
      char *end;
      result = strtod(string.c_str(), &end);
      if(*end == '\0') {
        if(semantically_else_syntactically) {
          if(result < min) {
            addCheckMessage(value, "out of bound");
            result = min;
          }
          if(result > max) {
            addCheckMessage(value, "out of bound");
            result = max;
          }
          if(!std::isnan(precision) && precision > 0) {
            double r = min + precision * rint((result - min) / precision);
            if(result != r) {
              addCheckMessage(value, "not a sampled value");
            }
            result = r;
          }
        }
      } else {
        addCheckMessage(value, "not a parsable number");
        result = NAN;
      }
    }
    stopCheckMessage();
    return result;
  }
  double NumericType::getDistance(JSON lhs, JSON rhs) const
  {
    double l = getValueAsDouble(lhs), r = getValueAsDouble(rhs);
    return std::isnan(l) || std::isnan(r) ? INFINITY : std::isnan(step) || precision == 0 ? fabs(l - r) : fabs(l - r) / step;
  }
  wjson::Value NumericType::getPath(JSON lhs, JSON rhs) const
  {
    if(!Type::justDone(lhs, rhs)) {
      NumericType::getDistance(lhs, rhs);
    }
    // Returns an atomic path [lhs, rhs]
    if(atomic || std::isnan(path_step)) {
      return Type::getPath(lhs, rhs);
    }
    // Builds the iterative step sampling
    double l = getValueAsDouble(lhs), r = getValueAsDouble(rhs);
    wjson::Value path;
    path[0] = lhs;
    if(l < r) {
      for(unsigned int k = 1; l < r; k++) {
        path[k] = l = fmin(r, l + path_step);
      }
    } else if(r < l) {
      for(unsigned int k = 1; r < l; k++) {
        path[k] = l = fmax(r, l - path_step);
      }
    }
    return path;
  }
  double NumericType::getNormalized(JSON value, bool with_zero) const
  {
    return value.get(zero, with_zero);
  }
  double NumericType::getNormalized(double value, bool with_zero) const
  {
    if(value <= min) {
      return min;
    } else if(value >= max) {
      return max;
    } else { // min < value < max
      double u = (2 * value - (min + max)) / (max - min);
      if(with_zero) {
        // Calculates once number of iteration to map zero on 0
        if(normalized_order == (unsigned int) -1) {
          double z_1 = (2 * zero - (min + max)) / (max - min), z_0 = (z_1 < 0 ? -1 : 1) * (sqrt(2) - 1);
          normalized_coeff[0] = z_0 / (1 - z_0 * z_0);
          for(normalized_order = 0; fabs(z_1) >= fabs(z_0); normalized_order++) {
            double z_2 = z_1 - normalized_coeff[0] * (1 - z_1 * z_1);
            aidesys::alert(!(z_2 < z_1), "illegal-argument", "in symboling::NumericType::getNormalized zero=%e is too close to value bounds [%e, %e] thus numerically instable", zero, min, max);
            z_1 = z_2;
          }
          normalized_coeff[1] = z_1 / (1 - z_1 * z_1);
        }
        // Applies the non-linear profile
        for(unsigned int l = 0; l <= normalized_order; l++) {
          u -= normalized_coeff[l == normalized_order ? 1 : 0] * (1 - u * u);
        }
      }
      return u;
    }
  }
  double NumericType::compare(JSON lhs, JSON rhs) const
  {
    double l = getValueAsDouble(lhs), r = getValueAsDouble(rhs);
    return !std::isnan(precision) && fabs(l - r) < precision ? 0 : l - r;
  }
  String NumericType::asString() const
  {
    static std::string string;
    string = Type::asString();
    string = string.substr(0, string.length() - 1);
    string += " parameters: " + Numeric::asString();
    string += "}";
    return string;
  }
}
