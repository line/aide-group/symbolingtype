#ifndef __symboling_MidiType__
#define __symboling_MidiType__

namespace symboling {
  /**
   * @class MidiType
   * @description Specifies a symbolic music type using a segment of the MIDI specification.
   * - This defines the following types:
   *   - `midi_score` which is the root type of a MIDI object.
   *   - `midi_(header|tracks|track|timeSignatures|timeSignature|keySignatures|keySignature|endOfTrackTicks|instrument|controlChanges|controlChangeList|notes|note|numeric)` which are the MIDI components of a MIDI object.
   *   - This thus does NOT define the whole MIDI specification.
   * - Conversion from MIDI files to JSON files is performed via the Javascript utilities:
   *   - `[.../midi2json.js file.mid > file.json](./global.html#midi2json)` outputing a `file.json` JSON file.
   *   - `[.../json2midi.js < file.json file.midi](./global.html#json2midi)` outputing a `file.mid` MIDI file.
   * - An example of music morphing is available on this[demo page](./musicmorphingdemo/au-clair-de-la-lune-frere-jacques.html), using a command that generates a `file1-file2.html` file showing a sequence of intermediate sounds between `file1` and `file2`:
   *   - `cd .../musicmorphingdemo ; [make](https://gitlab.inria.fr/line/aide-group/symboling/-/blob/main/public/musicmorphingdemo/makefile) [build|rebuild] i1=file1.mid i2=file2.mid`
   *
   *
   *
   * - The utility [timidity](https://wiki.archlinux.org/title/Timidity++) for MIDI audio conversion and [audacity](https://audacity.fr) audio editor are also used at the application level.
   *
   * @extends Type
   */
}

#endif
