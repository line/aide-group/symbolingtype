%
% This file is used in  
%   https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/etc/tex_to_pdf.sh
% and command are documented in http://aide-line.inria.fr/build/www/etc.html#.tex_to_pdf
%

% Defines a minimal layout for pdf display

\documentclass[a4paper,12pt,landscape,pdftoolbar=false,pdfmenubar=false]{article}
\pagestyle{empty} 
\topmargin 0cm \oddsidemargin 0cm \evensidemargin 0cm 
\setlength{\parindent}{0in} 
\setlength{\parskip}{3mm} 
\usepackage[margin=2cm]{geometry}

% Here are the used packages

\usepackage[utf8]{inputenc}
\DeclareUnicodeCharacter{00B0}{\textsuperscript{o}}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}\usepackage{amssymb}\usepackage{amsfonts}
\usepackage{array}
\usepackage{times}
\usepackage{color}
%\usepackage{algpseudocode}\usepackage{algorithm}
\usepackage{listings}

% Here is tha macro for conditional compilation

\def\aidebuild {}

% Here are some used commands

\newcommand{\deq}{\stackrel {\rm def}{=}} 
\newcommand{\eqline}[1]{~\vspace{0.3cm}\\\centerline{$#1$}\vspace{0.3cm}\\}
\newcommand{\tab}{\hphantom{6mm}}
\newcommand{\hhref}[1]{\href{#1}{#1}}

% Here are some homemade commands to define minimal slides

\newcommand{\slide}[2]{\clearpage\fbox{\parbox[t][16.5cm][t]{\textwidth}{\vspace{0.5cm}\centerline{\fontsize{40}{50}\selectfont {\bf #1}}\vspace{0.2cm}\huge~\\~{#2}}}\newpage}
\newcommand{\sitem}[1]{\\$\bullet$ {#1}}
\newcommand{\ssitem}[1]{\\\tab- {#1}}
\newcommand{\sfbox}[1]{\framebox[\textwidth]{{#1}}}
\newcommand{\sright}[1]{\begin{flushright}#1\tab\end{flushright}}
\newcommand{\scenter}[1]{\begin{center}#1\end{center}}
\newcommand{\stwo}[4]{\begin{tabular}{ll}\parbox{#1\textwidth}{#3}&\parbox{#2\textwidth}{#4}\end{tabular}}
\newcommand{\sappendix}{\appendix\clearpage\parbox[t][16.5cm][t]{\textwidth}{\vspace{8cm}\centerline{\fontsize{40}{50}\selectfont {\bf Appendix}}}}

\begin{document}

\vspace{4cm} \centerline{\Huge Symboling: Metrizable symbolic data structure}

\subsubsection*{Position of the problem: Context and basic mechanism}

The basic ingredient is to generate a metrizable symbolic data structure embedding (say, a ``symboling''). In the resulting metric space, the lever is the notion of editing distance, i.e., the fact that a symbolic data value is step by step edited in order to equal another value. Each elementary editing operation has an additive cost, yielding both a well-defined distance and geodesics, i.e. a minimal distance path from the initial value to the target value. The key point is that such distance depends on the data type\footnote{For instance, given a numeric value, it will depends on the chosen bounds, scale and precision.}. Moreover, given a data type, this specification includes the projection of a data value in the neighborhood of the data type region onto it, as developed in \cite{palaude_metrizable_2023}.

\subsubsection*{Implementation: Structuring via typed data value}

At the implementation level, the data object model, the DOM, is a {\tt \href{https://line.gitlabpages.inria.fr/aide-group/wjson/Value.html}{Value}} which is either (i) an atomic string (including string parsable as numeric or boolean value) or (ii) a \href{https://en.wikipedia.org/wiki/Record\_(computer\_science)}{record}, i.e., an unordered set of elements accessed by label\footnote{This corresponds to a semantic variant of a JSON data structure, called \href{https://line.gitlabpages.inria.fr/aide-group/wjson/\#semantic}{wJSON}, for which:
\\- Record name/value pairs are ordered, preserving the insertion order of record keys, or sorted in any application related order.
\\- An array of syntax {\tt [a b ...]} is equivalent and equal to a record {\tt \{ 0: a 1: b ...\}} indexed by consecutive non negative integer.
\\ - All atomic values cast from and onto string, 
\\ - The `empty` value corresponds to an undefined default value, and more generally any type is expected to have a default value.}. This forms an unbounded set of well-formed values.
\\ In order to structure the data, the basic construction is the notion of {\tt \href{./Type.html}{Type}}. The set of values of a given type defines a metric subspace, i.e., a region of the state space equipped with (i) a distance between two values, and (ii) a projector from a neighborhood of this subspace onto it. 
\\ It appears that manipulating symbolic data structure requires to specify both a syntactic and semantic projection:

\centerline{\includegraphics[width=0.75\textwidth]{./value-type-geometric-view.png}}

The key point is that in order to be able to compute a projection in a neighborhood of the semantic type region onto it, the value must fulfill some syntactic constraints for the calculation to be properly defined\footnote{For instance, considering the type of positive integer, a value is syntactically valid if the string representation parses to a numeric value, and is semantically valid if this value is a positive integer. If the string can not parse to a number, then any numeric operation will be undefined. If the string parses to a numeric value, it is easy to define how to project such real number onto a positive integer.}, such neighborhood is referred as the type syntactic space\footnote{A step further, the syntactic neighborhood may correspond to a more general super type, value syntactically valid being semantically valid with respect to this super type. Thanks to this, the distance from a value to the type region can be calculated with respect to the super type metric, providing that the projection corresponds to the shortest distance.}.

\subsubsection*{Implementation: Basic data type}

Thanks to the proposed design, the specification is modular and hierarchical:

On one hand, atomic types correspond to string, numeric or modal (generalization of Boolean) values, as in any usual language, but with enriched meta-values.

\begin{center} \begin{tabular}{|l|l|} \hline
\href{./StringType.html}{string} & This data type specifies a subset of strings. \\ \hline
\href{./ModalType.html}{modal} & This \href{./modaltype.pdf}{data type} specifies a level of truth between -1 (false), 0 (unknown) and 1 (true). \\ \hline
\href{./NumericType.html}{numeric} & This \href{./numerictype.pdf}{data type} specifies a numeric value with its related metadata (bounds, precision, unit, …). \\ \hline
\end{tabular} \end{center}

The present preliminary implementation only considers basic atomic type, but it would be very easy to include all usual \href{https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/}{data types}, used for instance in \href{https://www.w3.org/TR/owl2-primer/#Datatypes}{OWL}.

Furthermore, structured data types such as (i) date and or time, (ii) \href{https://en.wikipedia.org/wiki/Internationalized_Resource_Identifier}{IRI} including \href{https://en.wikipedia.org/wiki/Uniform_Resource_Identifier}{URI}, thus \href{https://en.wikipedia.org/wiki/URL}{URL}, (iii) \href{https://en.wikipedia.org/wiki/W3C_Geolocation_API}{geolocation}, (iv) \href{https://www.w3.org/TR/ltli}{human language tags and locale}, there is a one to one mapping between a string with a specified syntax, and the data record items. The \href{./RecordType.html#getValue}{RecordType} implementation provides an elementary of mechanism of parsing based on regular expressions, while existing middleware manage more sophisticated data.

On the other hand, compound types correspond to usual enumeration, ordered list, unordered set, or named tuples, i.e., record. 

\begin{center} \begin{tabular}{|l|l|} \hline
\href{./EnumType.html}{enum} & This data type specifies an enumeration of other values.\\ \hline
\href{./ListType.html}{list-of-*} & This data type specifies an ordered list of values.\\ \hline
\href{./SetType.html}{set-of-*} & This data type specifies an unordered set of values.\\ \hline
\href{./RecordType.html}{record} & This data type specifies a record of values, i.e., a named tuples.\\ \hline
\href{./Type.html#}{value} & This data type is the root data type that corresponds to any value. \\ \hline
\end{tabular} \end{center}

Specification details, and type parameterization is detailed in the source file documentation.

New types can be defined \href{./Type.html#declaration}{combining these ingredients, given specific parameters} or \href{./Type.html#derivation}{deriving new types}.


\newpage {\scriptsize \bibliographystyle{apalike}\bibliography{AIDE.bib,}
\end{document}
