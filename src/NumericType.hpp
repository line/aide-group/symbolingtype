#ifndef __symboling_NumericType__
#define __symboling_NumericType__

#include <math.h>
#include "Type.hpp"
#include "Numeric.hpp"

namespace symboling {
  /**
   * @class NumericType
   * @description Specifies a bounded finite precision numeric value.
   * @extends Type
   *
   * <a name="parameters"></a>
   * ## Type parameters
   * ### General parameter
   * - `name: ...` The type name. By default the type name is "numeric".
   * ### Numeric type meta-data
   * - `precision: ...` The positive numerical precision. Default is `NAN`, i.e., undefined.
   *   - This is the value under which two values are indistinguishable.
   *   - If `precision=1` and `min` and `max` are integer this means we consider an integer value.
   *   - The default means that the value is a constant, not to be adjusted.
   * - `min: ...` The numerical minimal value. Default is `-DBL_MAX`.
   * - `max: ...` The numerical maximal value. Default is `DBL_MAX`.
   * - `zero: ...` The numerical default and initial value.
   *   - This corresponds to the empty value.
   *   - The default is `(max + min)/2`.
   * - `step: ...` The positive sampling step, in order to define a local neighborhood size. Default is `NAN`, i.e., undefined.
   *   - It is used to weight the distance calculation.
   *    - The default corresponds to a value that is not be sampled.
   * - `unit: ...` The numerical unit name. Defualt is "", i.e., undefined.
   * ### Parameters for the geodesic calculation
   * - `atomic: ...`
   *   - If true returns only an atomic path with the two left-hand and right-hand elements. Default is true.
   *   - If false returns a path with all intermediate values, precision value by precision value if defined.
   *
   * <a name="specification"></a>
   * ## NumericType specification
   * <table class="specification">
   * <tr style='border: 1px solid black;'><th> Syntactic projection </th><td>
   * - Returns <tt>NAN</tt> if not a parsable number.
   * <br/> - Returns the <tt>zero</tt> if an empty value.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Semantic projection  </th><td>
   * - If the value is higher than the maximal value or lower than the minimal value it is projected on the related bound.
   * <br/> - If <tt>precision</tt> is defined the value is projected to the closest <tt>min + k precision</tt> value, <tt>k</tt> being an integer.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Distance value       </th><td>
   * The distance is calculated as a weighted value <tt>|lhs-rhs|/step</tt>.
   * <br/> - It is <tt>INFINITY</tt> if not both object are numeric.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Geodesic path        </th><td>
   * The geodesic is assumed to be atomic, i.e. of length 1 if value are equal and 2 if different.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Comparison           </th><td>
   * - Returns either <tt>lhs-rhs</tt> or
   * <br/> <tt>0</tt> if the precision is defined and <tt>|lhs-rhs| < precision</tt>.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Bounds           </th><td>
   * - The <tt>min</tt> and <tt>max</tt> values are the obvious bounds, if undefined the empty value is the unique bound.
   * </td></tr>
   * </table>
   *
   * ## Note
   *
   * - The numeric parameter values can also be given as a JSON structure, or a String.
   * - The [`getNormalized()`](#getNormalized) method allows to map the numerical parameter onto the `[-1, 1]` interval optionally with `zero` mapped to `0`.
   *
   * @param {JSON|String} parameters The type parameters.
   *
   */
  class NumericType: public Type, public stepsolver::Numeric {
    bool atomic;
    double path_step;
protected:
    // Projects any value to the closests numeric value
    virtual double getValueAsDouble(JSON value, bool semantically_else_syntactically = true) const;
public:
    NumericType(JSON parameter);
    NumericType(String parameter);
    NumericType(const char *parameter);

    virtual wjson::Value getValue(JSON value, bool semantically_else_syntactically = true) const;
    virtual double getDistance(JSON lhs, JSON rhs) const;
    virtual wjson::Value getPath(JSON lhs, JSON rhs) const;
    virtual double compare(JSON lhs, JSON rhs) const;

    /**
     * @function getNormalized
     * @memberof NumericType
     * @instance
     * @description Returns a normalized value given this type.
     * - It maps a numerical parameter in the `[min, max]` range onto the `[-1, 1]` interval.
     * - If done `with_zero` the `zero` mapped to `0`. Without this option the mapping is linear, while with this option the mapping is a monotonic combination of quadratic profiles.
     * @param {Value|double} value The input value.
     * @param {bool} [with_zero=true] Non-linear normalization to map the `zero` value on `0`.
     * @return {double} A value in the `[-1, 1]` range.
     */
    virtual double getNormalized(JSON value, bool with_zero = true) const;
    virtual double getNormalized(double value, bool with_zero = true) const;
private:
    mutable unsigned int normalized_order = -1;
    mutable double normalized_coeff[2] = { 0, 0 };
public:
    virtual String asString() const;
  };
}
#endif
