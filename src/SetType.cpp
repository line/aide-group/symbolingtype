#include "SetType.hpp"
#include "RecordType.hpp"
#include "Hungarian.h"

namespace symboling {
  static bool SetTypeRegistered = Type::addType("SetType", [] (JSON parameters) { return new SetType(parameters);
                                                });
  SetType::SetType(JSON parameters) : ListType(parameters)
  {}
  SetType::SetType(String parameters) : SetType(wjson::string2json(parameters))
  {}
  SetType::SetType(const char *parameters) : SetType(wjson::string2json(parameters))
  {}
  SetType::SetType() : SetType("")
  {}

  wjson::Value SetType::getValue(JSON value, bool semantically_else_syntactically) const
  {
    startCheckMessage();
    wjson::Value result, value0 = ListType::getValue(value, semantically_else_syntactically);
    // Defines an order to generate a canonical representation
    class Compare {
      const Type& itemType;
public:
      Compare(const Type& itemType) : itemType(itemType) {}
      bool operator()(const wjson::Value *lhs, const wjson::Value *rhs) const {
        return itemType.compare(*lhs, *rhs) < 0;
      }
    }
    compare(itemType);
    // Inserts all elements sorted according to the item type order and eleminates duplicates by equality
    std::map < const wjson::Value *, unsigned int, Compare > items(compare);
    for(unsigned int i = 0; i < value0.length(); i++) {
      auto it = items.find(&value0.at(i));
      if(it == items.end()) {
        items[&value0.at(i)] = i;
      } else {
        addCheckMessage(value, (String) (value0.at(i) == value0.at(it->second) ? "duplicates" : "indistinguishable values") + " between index %d and %d: '%s' == '%s'" + (value0.at(i) != value0.at(it->second) && dynamic_cast < const RecordType * > (&itemType) != NULL ? " (you may have forgotten to declare record's item types)" : ""), i, it->second, value0.at(i).asString().c_str(), value0.at(it->second).asString().c_str());
      }
    }
    // Reports results
    for(auto it = items.begin(); it != items.end(); ++it) {
      result.add(*it->first);
    }
    stopCheckMessage();
    return result;
  }
#define VERBOSE 0
  double SetType::getDistance(JSON lhs, JSON rhs) const
  {
    if(Type::justDone(lhs, rhs)) {
      return Type::getDistance(lhs, rhs);
    }
    // Builds the distance matrix adding empty values to compare insert/delete with editing solutions
    unsigned int dim = lhs.length() + rhs.length();
    costMatrix.clear();
    for(unsigned int i = 0; i < dim; i++) {
      std::vector < double > row;
      for(unsigned int j = 0; j < dim; j++) {
        double c = cost(j >= rhs.length() ? (i >= lhs.length() ? 'e' : 'd') : (i >= lhs.length() ? 'i' : 'e'), lhs, i, rhs, j);
        aidesys::alert(std::isnan(c), "illegal-state", "in symboling::SetType::getDistance spurious cost value (NAN) for type '" + getParameters().get("name", "") + "' item-type is '" + itemType.getParameters().get("name", "") + "'");
        row.push_back(c);
      }
      costMatrix.push_back(row);
    }
#if VERBOSE
    // Prints the cost matrix
    {
      for(unsigned int j = 0; j < dim; j++) {
        for(unsigned int i = 0; i < dim; i++) {
          printf("%s%f%s", i == 0 ? "\n\t" : " ", costMatrix[j][i], i == dim - 1 && j == dim - 1 ? "\n" : "");
        }
      }
    }
#endif
    // Calls the Hungarian algorithm
    HungarianAlgorithm HungAlgo;
    double distance = HungAlgo.Solve(costMatrix, assignment);
#if VERBOSE
    // Prints the assignements
    {
      printf("{ ");
      for(unsigned int i = 0; i < assignment.size(); i++) {
        printf("{ %d: '%s' %d: '%s' d: %.1f }", i, lhs.at(i).asString().c_str(), assignment[i], rhs.at(assignment[i]).asString().c_str(), costMatrix[i][assignment[i]]);
      }
      printf(" }\n");
    }
#endif
    Type::setDistance(lhs, rhs, distance);
    return distance;
  }
  wjson::Value SetType::getPath(JSON lhs, JSON rhs) const
  {
    if(!Type::justDone(lhs, rhs)) {
      SetType::getDistance(lhs, rhs);
    }
    wjson::Value path;
    path[0] = lhs;
    for(unsigned int i = 0, ik = 0, k = 1; i < assignment.size(); i++) {
      if(costMatrix[i][assignment[i]] > 0) {
        if(i >= lhs.length()) {
          // Inserts the element
          JSON path_ik = itemType.getPath(wjson::Value::EMPTY, rhs.at(assignment[i]));
          path[k] = path[k - 1];
          path[k].add(ik, path_ik.at(1));
          k++;
          for(unsigned int l = 2; l < path_ik.length(); l++) {
            path[k] = path[k - 1];
            path[k].set(ik, path_ik.at(l));
            k++;
          }
          ik++;
        } else if(assignment[i] >= (int) rhs.length()) {
          // Deletes the element
          JSON path_ik = itemType.getPath(lhs.at(i), wjson::Value::EMPTY);
          for(unsigned int l = 1; l < path_ik.length() - 1; l++) {
            path[k] = path[k - 1];
            path[k].set(ik, path_ik.at(l));
            k++;
          }
          path[k] = path[k - 1];
          path[k].add(ik, wjson::Value::EMPTY);
          k++;
        } else {
          // Replaces the element
          JSON path_name = itemType.getPath(lhs.at(i), rhs.at(assignment[i]));
          for(unsigned int l = 1; l < path_name.length(); l++) {
            path[k] = path[k - 1];
            path[k].set(ik, path_name.at(l));
            k++;
          }
          ik++;
        }
      } else {
        if(i < lhs.length() && assignment[i] < (int) rhs.length()) {
          ik++;
        }
      }
    }
    return path;
  }
  String SetType::asString() const
  {
    static std::string string;
    string = Type::asString();
    string = string.substr(0, string.length() - 1);
    // @tobedone
    string += "}";
    return string;
  }
}
