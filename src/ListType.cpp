#include "ListType.hpp"

namespace symboling {
  static unsigned int ipow(unsigned int base, unsigned int exp)
  {
    unsigned int result = 1;
    for(;;) {
      if(exp & 1) {
        result *= base;
      }
      exp >>= 1;
      if(!exp) {
        break;
      }
      base *= base;
    }
    return result;
  }
  static bool ListTypeRegistered = Type::addType("ListType", [] (JSON parameters) { return new ListType(parameters);
                                                 });
  ListType::ListType(JSON parameters) : Type(parameters.isMember("name") ? parameters : parameters.clone().set("name", "list-of-" + parameters.get("itemType", "value"))), itemType(Type::getType(parameters.get("itemType", "value"))), deleteCost(parameters.get("deleteCost", -1)), insertCost(parameters.get("insertCost", parameters.get("deleteCost", -1))), editCost(parameters.get("editCost", -1)), minLength(parameters.get("minLength", 0)), maxLength(parameters.get("maxLength", -1))
  {
    // Calculates bounds_count
    {
      unsigned int c = itemType.getBoundsCount(), l0 = parameters.get("minLength", 0), l1 = parameters.get("maxLength", l0);
      ipow_c = new unsigned int[l1 + 2];
      for(unsigned int l = 0; l <= l1 + 1; l++) {
        ipow_c[l] = l < l0 ? 0 : ipow(c, l);
      }
      // Tests if bounds count overflows (higher than 2^64 > e^44.361418)
      bounds_count = c == 0 ? 0 : c == 1 ? l1 - l0 + 1 : (l1 + 1) * (log_c = log(c)) - log(c - 1) > 44.361418 ? 0 :
                     (ipow_c[l1 + 1] - ipow_c[l0]) / (c - 1); // sum(c^l,l=l0..l1);
    }
  }
  ListType::ListType(String parameters) : ListType(wjson::string2json(parameters))
  {}
  ListType::ListType(const char *parameters) : ListType(wjson::string2json(parameters))
  {}
  ListType::ListType() : ListType("")
  {}
  ListType::~ListType()
  {
    delete[] ipow_c;
  }
  wjson::Value ListType::getValue(JSON value, bool semantically_else_syntactically) const
  {
    startCheckMessage();
    wjson::Value result;
    if(!value.isArray()) {
      addCheckMessage(value, "not an array");
      // Returns the value as a list if not the case, it may be list with an empty value,
      wjson::Value list;
      list[0] = value;
      result = ListType::getValue(list, semantically_else_syntactically);
    } else {
      // Checks the list length
      if(minLength > value.length() || (maxLength != (unsigned int) -1 && maxLength < value.length())) {
        addCheckMessage(value, "list length (= %d) out of bound", value.length());
      }
      // Normalizes each item given its type
      for(unsigned int i = 0; i < value.length(); i++) {
        result[i] = itemType.getValue(value.at(i), semantically_else_syntactically);
      }
    }
    stopCheckMessage();
    return result;
  }
  double ListType::getDistance(JSON lhs, JSON rhs) const
  {
    if(Type::justDone(lhs, rhs)) {
      return Type::getDistance(lhs, rhs);
    }
    // Calculates the editing distance table
    ni = lhs.length(), nj = rhs.length();
    delete[] d;
    d = new Operation[(ni + 1) * (nj + 1)];
    unsigned int ni1 = ni + 1, ni2 = ni1 + 1;
    {
      // Initializes the border with the cost of deleting and inserting all events
      d /*[0][0]*/[0] = Operation('0', 0, 0, 0);
      for(unsigned int i = 0, ij = 1; i < ni; i++, ij++) {
        d /*[i][0]*/[ij] = Operation('d', i, -1, d[i].cost + cost('d', lhs, i, rhs, -1));
      }
      for(unsigned int j = 0, ij = ni1; j < nj; j++, ij += ni1) {
        d /*[0][j]*/[ij] = Operation('i', -1, j, d[ij - ni1].cost + cost('i', lhs, -1, rhs, j));
      }
      // Iterative calculation of the editing distances
      for(unsigned int j = 0, ij = ni2; j < nj; j++, ij++) {
        for(unsigned int i = 0; i < ni; i++, ij++) {
          char what;
          double c_min;
          if(lhs.at(i) == rhs.at(j)) {
            c_min = d /*[i-1][j-1]*/[ij - ni2].cost, what = '=';
          } else {
            double
              c_e = cost('e', lhs, i, rhs, j) + d /*[i-1][j-1]*/[ij - ni2].cost,
              c_d = cost('d', lhs, i, rhs, -1) + d /*[i-1][j]*/[ij - 1].cost,
              c_i = cost('i', lhs, -1, rhs, j) + d /*[i][j-1]*/[ij - ni1].cost;
            if(c_e <= c_d) {
              c_min = c_e, what = 'e';
              if(c_i < c_e) {
                c_min = c_i, what = 'i';
              }
            } else {
              c_min = c_d, what = 'd';
              if(c_i < c_d) {
                c_min = c_i, what = 'i';
              }
            }
          }
          d /*[i][j]*/[ij] = Operation(what, what == 'i' ? -1 : i, what == 'd' ? -1 : j, c_min);
        }
      }
      // Return the minimal cost
      double distance = d /*[ni][nj]*/[ni + ni1 * nj].cost;
      Type::setDistance(lhs, rhs, distance);
      return distance;
    }
  }
  wjson::Value ListType::getPath(JSON lhs, JSON rhs) const
  {
    if(!Type::justDone(lhs, rhs)) {
      ListType::getDistance(lhs, rhs);
    }
    // Calculates the minimal cost operation's path backward through the distance table
    delete[] ij_path;
    ij_path = new unsigned int[ni + nj + 1];
    unsigned int ni1 = ni + 1, ni2 = ni1 + 1, ij = ni + ni1 * nj;
    ij_path[nij = 0] = ij;
    while(ij > 0) {
      char what;
      switch(what = d[ij].what) {
      case 'd':
        ij -= 1;
        break;
      case 'i':
        ij -= ni1;
        break;
      case 'e':
      case '=':
        ij -= ni2;
        break;
      default:
        aidesys::alert(true, "illegal-state", "in symboling::ListType::getPath 1/2 spurious operation #%c(%2d, %2d, %.0f)", d[ij].what, d[ij].i, d[ij].j, d[ij].cost);
        break;
      }
      ij_path[++nij] = ij;
    }
    // Calculates the edition path from lhs to rhs
    wjson::Value path;
    path[0] = lhs;
    for(int ij = nij - 1, k = 1, ik = 0, il = 0, ir = 0; ij >= 0; ij--) {
      Operation o = d[ij_path[ij]];
      switch(o.what) {
      case '=':
        ik++, il++, ir++;
        break;
      case 'e':
      {
        JSON path_ik = itemType.getPath(lhs.at(il), rhs.at(ir));
        for(unsigned int l = 1; l < path_ik.length(); l++) {
          path[k] = path[k - 1];
          path[k].set(ik, path_ik.at(l));
          k++;
        }
        ik++, il++, ir++;
        break;
      }
      case 'd':
      {
        JSON path_ik = itemType.getPath(lhs.at(il), wjson::Value::EMPTY);
        for(unsigned int l = 1; l < path_ik.length() - 1; l++) {
          path[k] = path[k - 1];
          path[k].set(ik, path_ik.at(l));
          k++;
        }
        path[k] = path[k - 1];
        path[k].add(ik, wjson::Value::EMPTY);
        il++, k++;
        break;
      }
      case 'i':
      {
        JSON path_ik = itemType.getPath(wjson::Value::EMPTY, rhs.at(ir));
        path[k] = path[k - 1];
        path[k].add(ik, path_ik.at(1));
        k++;
        for(unsigned int l = 2; l < path_ik.length(); l++) {
          path[k] = path[k - 1];
          path[k].set(ik, path_ik.at(l));
          k++;
        }
        ik++, ir++;
        break;
      }
      default:
        aidesys::alert(true, "illegal-state", "in symboling::ListType::getPath 2/2 spurious operation #%c(%2d, %2d, %.0f)", d[ij].what, d[ij].i, d[ij].j, d[ij].cost);
        break;
      }
    }
    return path;
  }
  double ListType::compare(JSON lhs, JSON rhs) const
  {
    JSON l = itemType.getValue(lhs), r = itemType.getValue(rhs);
    unsigned int l_l = l.length(), l_r = r.length();
    for(unsigned int i = 0; i < std::min(l_l, l_r); i++) {
      double c = itemType.compare(l.at(i), r.at(i));
      if(c != 0) {
        return c;
      }
    }
    return l_l < l_r ? -1 : l_l > l_r ? 1 : 0;
  }
  JSON ListType::getBound(unsigned int index) const
  {
    static wjson::Value bound;
    bound.clear();
    unsigned int c = itemType.getBoundsCount(), l0 = getParameters().get("minLength", 0), l1 = getParameters().get("maxLength", l0);
    if(c > 0) {
      if(c > 1) {
        unsigned int l = (int) (log(ipow_c[l0] + index * (c - 1)) / log_c); // solve(sum(c^l,l=l0..l-1)=i, l);
        if(l <= l1) {
          index -= (ipow_c[l] - ipow_c[l0]) / (c - 1); // sum(c^l,l=l0..l-1);
          for(unsigned int i = 0; i < l; i++) {
            unsigned int index_i = index % c;
            bound.add(itemType.getBound(index_i));
            index /= c;
          }
        }
      } else {
        if(l0 + index <= l1) {
          for(unsigned int i = 0; i < l0 + index; i++) {
            bound.add(itemType.getBound(0));
          }
        }
      }
    }
    return bound;
  }
  String ListType::asString() const
  {
    static std::string string;
    string = Type::asString();
    string = string.substr(0, string.length() - 1);
    if(d != NULL) {
      // Prints the edit distance table
      string += aidesys::echo("\n edit-distance-table: \" // Table (%d x %d) [#what(lhs_index, rhs_index, cumulative_cost ...]:\n\t", ni, nj);
      for(unsigned int j = 0, ij = 0; j <= nj; j++) {
        for(unsigned int i = 0; i <= ni; i++, ij++) {
          string += aidesys::echo("#%c(%2d, %2d, %.0f)%s", d[ij].what, d[ij].i, d[ij].j, d[ij].cost, i < ni ? " " : j < nj ? "\n\t" : "\"\n");
        }
      }
      if(ij_path != NULL) {
        // Reduces equalities in the operation's path before printing
        for(unsigned int ij0 = 0, ij1 = 0; ij0 <= nij; ij0++, ij1++) {
          if(d[ij_path[ij0]].what == '=') {
            ij1++, nij--;
          }
          ij_path[ij0] = ij_path[ij1];
        }
        // Prints the operation's path vector
        string += aidesys::echo(" edit-distance-path: \" // Path (length = %d) transitions from rhs back to lhs:\n", nij + 1);
        for(unsigned int ij = 0; ij < nij; ij++) {
          string += aidesys::echo("\t#%c(%2d, %2d, %.0f)%s", d[ij_path[ij]].what, d[ij_path[ij]].i, d[ij_path[ij]].j, d[ij_path[ij]].cost, ij < nij - 1 ? "\n" : "\"\n");
        }
      }
    }
    string += "}";
    return string;
  }
  double ListType::cost(char what, JSON lhs, unsigned int i, JSON rhs, unsigned int j) const
  {
    switch(what) {
    case 'd':
      return deleteCost >= 0 ? deleteCost : itemType.getDistance(itemType.getValue(lhs.at(i)), itemType.getValue(wjson::Value::EMPTY));
    case 'i':
      return insertCost >= 0 ? insertCost : itemType.getDistance(itemType.getValue(wjson::Value::EMPTY), itemType.getValue(rhs.at(j)));
    case 'e':
      return lhs.at(i) == rhs.at(j) ? 0 : editCost >= 0 ? editCost : itemType.getDistance(itemType.getValue(lhs.at(i)), itemType.getValue(rhs.at(j)));
    default:
      aidesys::alert(true, "illegal-argument", "in symboling::ListType::cost undefined what=%c action", what);
      return -1;
    }
  }
}
