#!/usr/bin/env node
let e=require("svg-json-parser");function n(e){e={tag:"svg",attr:{viewBox:"0 0 500 500"},children:JSON.parse(e)},t(e),u(e),s(e),e=`<?xml version="1.0" encoding="utf-8"?>
`+r(e);return e}function r({tag:e,attr:n,children:t}){return`<${e} ${o(n)}>

${t?t.map(r).join("\n"):""}

</${e}>`}function o(e){var n=[];for(attribute in e){var t=`${attribute}="${e[attribute]}"`;n.push(t)}return n.join(" ")}function t(e){if("object"==typeof e)for(element in e.style&&(e.style=i(e.style)),e)t(e[element])}function i(e){var n=[];for(prop in e)n.push(`${prop}: ${f(e[prop],prop)}; `);return n.join(" ")}function f(e,n){return 0===e.r&&0===e.g&&0===e.b&&e.a==("stroke"===n)?"none":`rgb(${e.r},${e.g},${e.b})`}function u(e){if("object"==typeof e)for(element in e.d&&(e.d=c(e.d)),e)u(e[element])}function c(e){try{return e.map(e=>e.type+" "+e.segment.join(" ")).join(" ")}catch(e){return""}}function s(e){if("object"==typeof e)for(element in e.transform&&(e.transform=a(e.transform)),e)s(e[element])}function a(e){return console.log(e),`matrix(${e.join(", ")})`}let l=n;console.log(l(require("fs").readFileSync(require("process").stdin.fd,"utf-8")));
