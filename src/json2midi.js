#!/usr/bin/env node

/**
 * @function json2midi
 * @static
 * @description Converts a file from JSON to MIDI format
 * ```
 *     Usage: ./src/json2midi.js < input output_file
 * ```
 * - Installation requires: `npm install [@tonejs/midi](https://www.npmjs.com/package/@tonejs/midi)`.
 */

const {
  Midi
} = require("@tonejs/midi");
const fs = require("fs");

function convertJSONToMidi(input, output_file) {
  setTimeout(() => {
    console.error("In json2midi.js, unable to construct '" + output_file + "' from input, process too long");
    process.exit(0);
  }, 60 * 1000);
  try {
    const jsonData = JSON.parse(input);
    const midi = new Midi();
    midi.fromJSON(jsonData);
    const data = midi.toArray();
    fs.writeFileSync(output_file, data);
  } catch (error) {
    console.error("In json2midi.js, unable to construct '" + output_file + "' from input, spurious format detected");
  }
  process.exit(0);
}
if (process.argv.length == 3) {
  convertJSONToMidi(fs.readFileSync(require("process").stdin.fd, "utf-8"), process.argv[2]);
} else {
  console.log("Usage " + process.argv[1] + " < input output_file");
  process.exit(-1);
}
