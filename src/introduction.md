@aideAPI

@slides ./symboling.pdf

- [A demo of visualmorphing to illustrate a symbolic geodesic](./visualmorphingdemo/titi-toto.html)

- [A demo of soundmorphing to illustrate a symbolic geodesic](./musicmorphingdemo/au-clair-de-la-lune-frere-jacques.html)




