# symbolingtype

Symbolic data structure type specification

@aideAPI

@slides ./symboling.pdf

- [A demo of visualmorphing to illustrate a symbolic geodesic](./visualmorphingdemo/titi-toto.html)

- [A demo of soundmorphing to illustrate a symbolic geodesic](./musicmorphingdemo/au-clair-de-la-lune-frere-jacques.html)





<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/symbolingtype'>https://gitlab.inria.fr/line/aide-group/symbolingtype</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/symbolingtype'>https://line.gitlabpages.inria.fr/aide-group/symbolingtype</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/symbolingtype/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/symbolingtype/-/tree/master/src</a>
- Saved on <a target='_blank' href='https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.inria.fr/line/aide-group/symbolingtype'>softwareherirage.org</a>
- Version `0.1.0`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/symbolingtype.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>@tonejs/midi: <a target='_blank' href='https://tonejs.github.com/Midi/'>Convert binary midi into JSON</a></tt>
- <tt>aidesys: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidesys'>Basic system C/C++ interface routines to ease multi-language middleware integration</a></tt>
- <tt>npm: <a target='_blank' href='https://docs.npmjs.com/'>a package manager for JavaScript</a></tt>
- <tt>stepsolver: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/stepsolver'>A step by step variational solver mechanism</a></tt>
- <tt>svg-json-parser: <a target='_blank' href='https://www.npmjs.com/package/svg-json-parser'>converting SVG Elements to JSON Object</a></tt>
- <tt>wjson: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/wjson'>Implements a JavaScript JSON weak-syntax reader and writer</a></tt>

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Authors

- Paul Bernard&nbsp; <big><a target='_blank' href='mailto:pbernard007@ensc.fr'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://www.linkedin.com/in/paul-bernard-3a56b2201'>&#128463;</a></big>
- Benjamin Haté&nbsp; <big><a target='_blank' href='mailto:bhate@ensc.fr'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://www.linkedin.com/in/benjamin-hat%C3%A9-27b754202'>&#128463;</a></big>
- Thierry Viéville&nbsp; <big><a target='_blank' href='mailto:thierry.vieville@inria.fr'>&#128386;</a></big>
