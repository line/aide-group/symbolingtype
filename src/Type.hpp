#ifndef __symboling_Type__
#define __symboling_Type__

#include "Value.hpp"
#include <map>
#ifndef SWIG
#include <functional>
#endif

namespace symboling {
  /**
   * @class Type
   * @description Implements a type on a structured hierarchical value.
   *
   * See the [introduction](./index.html) for a [general presentation](./symboling.pdf).
   *
   * <a name="declaration"></a>
   * ## Type declaration
   * - A typical declaration of a type "SomeType" and of name "mytype" with dedicated parameters is given by one of the following two constructs:
   *   - Here the myType is a static variable in order to be registered during the whole program execution.
   * ```
   * static symboling::SomeType myType("{name: myType ...}");
   * ```
   *   - Here myType is allocated and remains allocated during the whole program execution, and deleted at program shutdown.
   * ```
   * Type::addType(new SomeType("{name: myType ...}"));
   * ```
   *  - Predefined parameterized types built on `(String|Numeric|Modal|Enum|List|Set|Record)Type` can also be defined as data structure, e.g., for a `RecordType`:
   * ```
   * Type::addType("{type: record name: myType ...}");
   * ```
   *
   * ## Predefined standard types
   * - "value": that matches any predefined value, without any [further structure](#specification).
   * - "modal": that corresponds to the basic implementation of the [ModalType](./ModalType.html).
   * - "string": that corresponds to the basic implementation of the [StringType](./StringType.html).
   *
   * <a name="derivation"></a>
   * ## Implementation of a derived types
   * A type defines
   * - [`getValue(value, false)`](#getValue): A syntactic projection from any value to a value that can be manipulated in a neighborhood of this type region.
   * - [`getValue(value, true)`](#getValue): A semantic projection from a syntactically correct value onto a semantically correct value within the state space region.
   * - [`getCheckMessage()`](#getCheckMessage): A human readable report of what was missing in order the value to be syntactically or semantically correct, during the last `getValue()` call.
   * - [`getDistance(value_1, value_2)`](#getDistance): A distance between two semantically correct values.
   * - [`getPath(value_1, value_2)`](#getPath): A geodesic, i.e. a path of minimal distance, between two semantically correct values.
   * - [`compare(value_1, value_2)`](#compare): A comparison mechanism between two values of a given type.
   *
   * - A typical implementation of a derived  type of name "mytype" is given by the following construct.
   *   - Here the `justDone()`, `setDistance()` and `getDistance()` constructs allow the implementation to always have the distance well defined before calculating the path.
   *   - Here the `clearCheckMessage()` and `addCheckMessage(value, message_a_la_printf, ...)`, constructs allow to reset and append a standard error message, optionally with additional parameters using the printf syntax.
   * ```
   * #include "Type.hpp"
   * namespace symboling {
   *   class MyType : public Type {
   *   public:
   *     MyType(JSON parameters) : Type("{name: MyType, parameter: value, ../..}") {
   *       ../..
   *     }
   *     MyType(String parameters) : MyType(wjson::string2json(parameters)) {}
   *     MyType(const char *parameters) : Type(wjson::string2json(parameters)) {}
   *
   *     virtual wjson::Value getValue(JSON value, bool semantically_else_syntactically = true) const {
   *       startCheckMessage(); // Opens the check message mechanism
   *       wjson::Value result = value; // Calculates the semantically or syntactically correct value.
   *       ../..
   *         addCheckMessage(value, "explanation", ...); // Adds some human readable message about the required modification if needed, using a à-la printf format
   *       ../..
   *       stopCheckMessage(); // Closes the check message mechanism
   *       return result;
   *     }
   *
   *     virtual double getDistance(JSON lhs, JSON rhs) const {
   *       // Cache mechanism, returns the distance without recalculating it
   *       if (Type::justDone(lhs, rhs)) return Type::getDistance(lhs, rhs);
   *       // Otherwise, calculates the distance
   *       double distance = NAN;
   *       ../..
   *       Type::setDistance(lhs, rhs, distance); // Registers the distance for a next call.
   *       return distance;
   *     }
   *
   *     virtual wjson::Value getPath(JSON lhs, JSON rhs) const {
   *       // Cache mechanism, calculates the distance 1st if not yet done
   *       if (!Type::justDone(lhs, rhs)) MyType::getDistance(lhs, rhs);
   *       // Then, calculates the path
   *       ../..
   *       return path;
   *     }
   *
   *     virtual int compare(JSON lhs, JSON rhs) const {
   *       // Implements the semi-order comparison between two values
   *     }
   *
   *     virtual JSON getBound(unsigned int index) const {
   *       // Implements the dynamic calculation of the type bound of a given index
   *       // Alternate solution, using the Type::getBound() without overwritting:
   *       //  - The protected variable `bounds_count` must be calculated once, at the object construction.
   *       //  - The protected variable `bounds` allows to store the list of bound values, if not too many.
   *     }
   *
   *     virtual String asString() {
   *       static std::string string;
   *       string = Type::asString();
   *       // Adds complementary information related to MyType parameters and state
   *       ../..
   *       return string;
   *     }
   *
   *   } myType;
   *   // Allows to register a type from a JSON specification
   *   static bool MyTypeRegistered = Type::addType("MyType", [](JSON parameters) { return new MyType(parameters); });
   * }
   * ```
   *
   * <a name="specification"></a>
   * ## ValueType specification
   * <table>
   * <tr style='border: 1px solid black;'><th> Syntactic projection </th><td>
   * - Returns the value itself, no projection at this level of specification.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Semantic projection  </th><td>
   * - Returns the value itself, no projection at this level of specification.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Distance value       </th><td>
   * - Returns <tt>NAN</tt> no distance defined at this level of specification.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Geodesic path        </th><td>
   * - The geodesic is assumed to be atomic, i.e. of length 1 if value are equal and 2 if different.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Comparison           </th><td>
   * - Returns <tt>NAN</tt> no comparison possible at this level of specification.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Bounds           </th><td>
   * - The empty value is the unique bound a this level of specification.
   * </td></tr>
   * </table>
   *
   * @param {JSON|String} parameters The type parameters.
   * - By contract, the type name is a mandatory parameter, default is "value".
   *   - Note: Since each name has a different type it used to define hash, less and equal function to index type in containers, it is possible to define `std::unordered_map<symboling::Type, T>` or `std::map<symboling::Type, T>`.
   */
  class Type {
    wjson::Value parameters;
    static std::string check_message;
    static unsigned int check_message_recursion_level;
    mutable wjson::Value last_lhs, last_rhs;
    mutable bool already_set = false;
    mutable double last_distance = 0;
protected:
    // Used to implement the check mechanims
    static void startCheckMessage();
    void addCheckMessage(JSON value, String explanation, ...) const;
    static void stopCheckMessage();
    // Used to implement the getDistance() and getPath() methods
    bool justDone(JSON lhs, JSON rhs) const;
    void setDistance(JSON lhs, JSON rhs, double distance) const;
public:
    Type(JSON parameters);
    Type(String parameters);
    Type(const char *parameters);
    virtual ~Type() {}

    // Avoids copy of types
    Type(const Type& obj);
#ifndef SWIG
    Type& operator = (const Type& type);
#endif

    /**
     * @function getParameters
     * @memberof Type
     * @instance
     * @description Returns the type parameters.
     * @return {JSON} The type parameters.
     */
    JSON getParameters() const
    {
      return parameters;
    }
    bool operator == (const Type& type) const {
      return parameters.get("name", "") == type.parameters.get("name", "");
    }
    bool operator < (const Type& type) const {
      return parameters.get("name", "") < type.parameters.get("name", "");
    }

    /**
     * @function getValue
     * @memberof Type
     * @instance
     * @description Projects any value on the closest value of this type.
     * @param {JSON|String} value The input value.
     * @param {bool} [semantically_else_syntactically=true]
     * - If true projects the value onto a semantically valid value with respect to this type.
     * - If false projects the value onto a syntactically valid value with respect to this type.
     * @return {Value} The projection of the value onto the a value of this type:
     * - If the value is already of this type it is unchanged.
     * - If the value can be mapped on a value of this type, then it is returned.
     * - Otherwise a default "undefined" value or an empty value is returned.
     */
    virtual wjson::Value getValue(JSON value, bool semantically_else_syntactically = true) const;
    virtual wjson::Value getValue(String value, bool semantically_else_syntactically = true) const;
    virtual wjson::Value getValue(const char *value, bool semantically_else_syntactically = true) const;

    /**
     * @function getCheckMessage
     * @memberof Type
     * @instance
     * @description Returns the getValue call check message.
     * @param {Value} [value] If specified returns the getValue call check message for this value, otherwise return the last getValue() evaluation check message.
     * @return {string} A human readable report of what was missing in order the value to be syntactically or semantically correct, during the last getValue call.
     * - Empty, i.e. equal to "" if the value fits this type,
     * - Otherwise, a human readable multi-line string with explicit errors of the form:
     *   - `// The '$copy-of-the-value' is not of type '$type-name': $explanation`
     */
    std::string getCheckMessage(JSON value) const
    {
      getValue(value);
      return check_message;
    }
    std::string getCheckMessage() const
    {
      return check_message;
    }
    /**
     * @function getDistance
     * @memberof Type
     * @instance
     * @description Returns the distance between two values of this type.
     *  -
     * @param {Value} lhs The left hand-size value.
     * @param {Value} rhs The right hand-size value.
     * @return {double} The positive value of the distance or:
     * - <tt>0</tt> if the distance itself is undefined, i.e., all values are indistinguishable. This is the default behavior.
     * - <tt>DBL_MAX</tt>, for infinity, if values are not comparable or unrelated, thus at infinite distance.
     */
    virtual double getDistance(JSON lhs, JSON rhs) const;

    /**
     * @function getPath
     * @memberof Type
     * @instance
     * @description Returns a geodesic, i.e. a path of minimal distance between two values of this type.
     * - By contract, a cache mechanism is to be used, to properly calculates the distance before calculating the path, see the use of`justDone()`, `setDistance()` and `getDistance()`, exemplified above.
     * @param {Value} lhs The left hand-size value.
     * @param {Value} rhs The right hand-size value.
     * @return {Array<string>} The list of intermediate values between left and right hand-size values.
     * - If lhs = rhs returns a path of length 1.
     * - If lhs and rhs differs only by an atomic change returns a path of length 2. This is the default behavior.
     * - Usually, several geodesics correspond to the minimal distance and the method is expected to return the ``1st one ´´ in a sense that is to make explicit for each data type.
     */
    virtual wjson::Value getPath(JSON lhs, JSON rhs) const;

    /**
     * @function compare
     * @memberof Type
     * @instance
     * @description Returns a semi-order comparison between two values of this type.
     * @param {Value} lhs The left hand-size value.
     * @param {Value} rhs The right hand-size value.
     * @return {double} The comparison value:
     * - `<0` if lhs compares less than rhs.
     * - `>0` if lhs compares more than rhs.
     * - `0` if both value are too close to be comparable.
     * - `NAN` if both value can not be compared, this is the default.
     */
    virtual double compare(JSON lhs, JSON rhs) const;

    /**
     * @function getBound
     * @memberof Type
     * @instance
     * @description Returns the one value bound for this data type.
     * - The `uint getBoundsCount()` function returns the precise maximal number of bounds for this type.
     *   - If the bounds count overflows (above 2^64) then it is set to 0, so that no bounds are available.
     * @param {uint} index The bound index.
     * @return {Value} The bound value or the empty undefined value if out of bound.
     */
    virtual JSON getBound(unsigned int index) const;
    unsigned int getBoundsCount() const
    {
      return bounds_count;
    }
protected:
    wjson::Value bounds;
    unsigned int bounds_count;
public:

    /**
     * @function getBounds
     * @memberof Type
     * @instance
     * @description Returns a list of value bounds for this data type.
     * @param {uint} bounds_count The maximal number of bounds. If `bounds_count = 0` returns all bounds of the type.
     * @param {bool} [reset = false]
     * - If true, redraw the random bound selection.
     * - If false, returns the last bounds calculation result, if with the same bounds_count.
     * @return Returns a list of at most bounds_count bounds, randomly selected if `bounds_count < getBoundsCount()`.
     */
    JSON getBounds(unsigned int bounds_count = 0, bool reset = false) const;
private:
    mutable wjson::Value last_bounds;
    mutable unsigned int last_bounds_count = -1;
public:

    /** Returns this type parameters and current state as a string, for debugging purpose.
     * - The default implementation reports the type parameters and the last distance value if any.
     * @return {String} The type parameters and current state as a string.
     */
    virtual String asString() const;

    /**
     * @function getType
     * @memberof Type
     * @static
     * @description Returns a type of the given name.
     * @param {String} name The type name.
     * @return {Type} The corresponding type if any, else the generic type of name "value".
     */
    static const Type& getType(String name);

    /**
     * @function getTypeNames
     * @memberof Type
     * @static
     * @description Returns the list of all type names.
     * @return {Value} The list of all type names.
     */
    static wjson::Value getTypeNames();

    /**
     * @function getTypeParameters
     * @memberof Type
     * @static
     * @description Returns the list of all type parameters.
     * @return {Value} The list of all type parameters.
     */
    static wjson::Value getTypeParameters();

    /**
     * @function addType
     * @memberof Type
     * @static
     * @description Adds a new type from deriving some type with dedicated parameters.
     * A typical construct is of the form:
     * ```
     * Type::addType(new SomeType("{name: myType ...}"));
     * ```
     * @param {Type} type The pointer of the type to add.
     * - The object is deleted on program shutdown.
     * - The type·s can be defined as
     *    - A JSON data structure, e.g. of the form `{type: record name: myType ...}`, including as a string, for type parametrizing  `(String|Numeric|Modal|Enum|List|Set|Record)Type` etc.
     *    - A JSON list of data structures, e.g. of the form `[{type: record name: myType ...}`, including as a string.
     */
    static void addType(Type *type);
    static void addType(JSON type);
    static void addType(String type);
    static void addType(const char *type);
#ifndef SWIG
    static bool addType(String name, std::function < Type *(JSON) > type);
#endif
private:
    static std::map < std::string, Type * > &getTypes();
#ifndef SWIG
    static std::map < std::string, std::function < Type *(JSON) >> typeFactory;
#endif
  };
}
#ifndef SWIG
// Defines a hash function to use Type in unordered_map container
template < > struct std::hash < symboling::Type > {
  size_t operator()(const symboling::Type & atype) const {
    return std::hash < std::string > {}
           (atype.getParameters().get("name", ""));
  }
};
#endif
#endif
