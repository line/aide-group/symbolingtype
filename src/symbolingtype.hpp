#ifndef __symboling__
#define __symboling__

#include "Type.hpp"
#include "StringType.hpp"
#include "NumericType.hpp"
#include "ModalType.hpp"
#include "EnumType.hpp"
#include "RecordType.hpp"
#include "ListType.hpp"
#include "SetType.hpp"

#include "SvgType.hpp"
#include "MidiType.hpp"

#endif
