#include "EnumType.hpp"
#include "random.hpp"

namespace symboling {
  static bool EnumTypeRegistered = Type::addType("EnumType", [] (JSON parameters) { return new EnumType(parameters);
                                                 });
  EnumType::EnumType(JSON parameters) : Type(parameters), itemType(Type::getType(parameters.get("itemType", "value")))
  {
    // Here each prototype value is reprojected on the data type.
    wjson::Value& values = const_cast < wjson::Value & > (getParameters().at("values"));
    bounds.clear();
    for(unsigned int i = 0; i < values.length(); i++) {
      values[i] = itemType.getValue(values.at(i));
      bounds.add(values.at(i));
    }
    bounds_count = values.length();
  }
  EnumType::EnumType(String parameters) : EnumType(wjson::string2json(parameters))
  {}
  EnumType::EnumType(const char *parameters) : EnumType(wjson::string2json(parameters))
  {}
  EnumType::EnumType() : EnumType("")
  {}

  wjson::Value EnumType::getInterpolatedValue(JSON value, JSON parameters) const
  {
    unsigned int p = parameters.get("p", (unsigned int) (aidesys::random() * parameters.at("value").length()));
    JSON v1 = parameters.at("value").at(p);
    JSON v0 = itemType.getValue(value, false);
    JSON path = itemType.getPath(v0, v1);
    unsigned int l = path.length();
    unsigned int q = std::min(parameters.get("q", (unsigned int) (aidesys::random() * l)), l - 1);
    if(parameters.isMember("r") && l > 2) {
      q = q == 0.0 ? 0 : q == 1.0 ? l - 1 : std::max((unsigned int) 1, std::min(l - 2, (unsigned int) rint(parameters.get("r", 0.0) * l)));
    }
    return path.at(q);
  }
  wjson::Value EnumType::getValue(JSON value, bool semantically_else_syntactically) const
  {
    startCheckMessage();
    wjson::Value result, value_0 = itemType.getValue(value, semantically_else_syntactically);
    lastDistance = INFINITY;
    for(unsigned int i = 0; lastDistance > 0 && i < getParameters().at("values").length(); i++) {
      JSON value_i = getParameters().at("values").at(i);
      double d = itemType.getDistance(value_0, value_i);
      if(i == 0 || d < lastDistance) {
        lastDistance = d, result = value_i;
      }
    }
    if(lastDistance > 0) {
      addCheckMessage(value, "not in the enumeration list");
    }
    stopCheckMessage();
    return result;
  }
  double EnumType::getDistance(JSON lhs, JSON rhs) const
  {
    if(Type::justDone(lhs, rhs)) {
      return Type::getDistance(lhs, rhs);
    }
    // Return the minimal cost
    double distance = itemType.getDistance(EnumType::getValue(lhs), EnumType::getValue(rhs));
    Type::setDistance(lhs, rhs, distance);
    return distance;
  }
  wjson::Value EnumType::getPath(JSON lhs, JSON rhs) const
  {
    if(!Type::justDone(lhs, rhs)) {
      EnumType::getDistance(lhs, rhs);
    }
    wjson::Value path = itemType.getPath(EnumType::getValue(lhs), EnumType::getValue(rhs));
    return path;
  }
  double EnumType::compare(JSON lhs, JSON rhs) const
  {
    return itemType.compare(EnumType::getValue(lhs), EnumType::getValue(rhs));
  }
  String EnumType::asString() const
  {
    static std::string string;
    string = Type::asString();
    string = string.substr(0, string.length() - 1);
    // @tobedone
    string += "}";
    return string;
  }
}
