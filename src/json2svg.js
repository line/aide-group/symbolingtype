#!/usr/bin/env node

/**
 * @function json2svg
 * @static
 * @description Converts a file from JSON to SVG format
 * ```
 *     Usage: ./src/json2svgsvg2json.js < input > output
 * ```
 * - Installation requires: `npm install [svg-json-parser](https://www.npmjs.com/package/svg-json-parser)`.
 */

const svg_json_parser = require('svg-json-parser');

function JsonToSVG(jsonStr) {
  let object = {
    "tag": "svg",
    "attr": {
      "viewBox": "0 0 500 500"
    },
    "children": JSON.parse(jsonStr)
  }
  stringifyStyles(object);
  stringifyPaths(object);
  stringifyTransforms(object);
  let svg = `<?xml version="1.0" encoding="utf-8"?>\n` + toSVG(object)
  return svg
}

function toSVG({
  tag,
  attr,
  children
}) {
  return `<${tag} ${attributeString(attr)}>\n\n${children ? children.map(toSVG).join('\n') : ''}\n\n</${tag}>`
}

function attributeString(attributes) {
  let attributeStringList = [];
  for (attribute in attributes) {
    let attributeString = `${attribute}="${attributes[attribute]}"`;
    attributeStringList.push(attributeString)
  }
  return attributeStringList.join(" ");
}

function stringifyStyles(obj) {
  if (typeof(obj) !== 'object') return
  if (obj['style']) {
    obj.style = stringifyStyle(obj['style']);
  }
  for (element in obj) {
    stringifyStyles(obj[element]);
  }
}

function stringifyStyle(style) {
  let propstring = []
  for (prop in style) {
    propstring.push(`${prop}: ${stringifyColor(style[prop], prop)}; `)
  }
  return propstring.join(' ');
}

function stringifyColor(c, prop) {
  if (c.r === 0 && c.g === 0 && c.b === 0 && (c.a == (prop === "stroke"))) return "none";
  return `rgb(${c.r},${c.g},${c.b})`
}

function stringifyPaths(obj) {
  if (typeof(obj) !== 'object') return
  if (obj['d']) {
    obj.d = stringifyPath(obj['d']);
  }
  for (element in obj) {
    stringifyPaths(obj[element]);
  }
}

function stringifyPath(path) {
  try {
    return path
      .map(segment => {
        return `${segment.type} ${segment.segment.join(' ')}`
      })
      .join(' ');
  } catch (error) {
    return "";
  }
}

function stringifyTransforms(obj) {
  if (typeof(obj) !== 'object') return
  if (obj['transform']) {
    obj.transform = stringifyMatrix(obj['transform']);
  }

  for (element in obj) {
    stringifyTransforms(obj[element]);
  }
}

function stringifyMatrix(matrixArray) {
  console.log(matrixArray)
  return `matrix(${matrixArray.join(', ')})`
}

const converter = JsonToSVG;

// Standard command to convert a file from a format to another
console.log(converter(require("fs").readFileSync(require("process").stdin.fd, "utf-8")));
